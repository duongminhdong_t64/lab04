<?php
$gioiTinh = array(
    0 => 'Nam',
    1 => 'Nữ'
);
$khoa = array(
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="lab03.css">
</head>

<body>
    <?php
    $name = "none";
    $date = "none";
    $gender = "none";
    $faculty = "none";
    $address = "none";
    function emptyInputFeild($input)
    {
       
        return $input == '';
    }


    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (emptyInputFeild($_POST["name"])) {
            echo "<div style='color: red;'>Hãy nhập họ tên của bạn . </div>";
        }
        if (!isset($_POST['gender'])) {
            
            echo "<div style='color: red;'>Hãy chọn giới tính của bạn .</div>"; // echo the choice 
        }
        if (emptyInputFeild($_POST["faculty"])) {
            echo "<div style='color: red;'>Hãy chọn phân khoa .</div>";
        }
        if (emptyInputFeild($_POST["date"])) {
            echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
        } elseif(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$_POST["date"])){
            echo "<div style='color: red;'> Hãy nhập ngày sinh đúng định dạng </div>";
        }
        if (emptyInputFeild($_POST["address"])) {
            echo "<div style='color: red;'>Hãy nhập địa chỉ của bạn .</div>";
        }
    }








    ?>
    <div id='backDiv'>
        <form method="POST" style="position: center !important;
    width:80%;
    margin-left: 10%;
    margin-right: 10%;">
            <!-- <p style="color:red;"> Hãy nhập tên . </p>
            <p style="color:red;"> Hãy nhập ngày sinh . </p> -->
            <div id="nameDiv" class="required-field">
                <label id="label" class="h-100" for="name">Họ và tên </label>
                <input id="input" class="h-100" type="text" name="name">
                <!-- required="" placeholder="Dương Minh Đông" oninvalid="this.setCustomValidity('Hãy nhập tên của bạn')" oninput="setCustomValidity('')" -->
            </div>
            <div id="infoDiv">
                <label id="label" class="h-100"> Giới tính </label>
                <div id="radioBtn">
                    <?php
                    for ($i = 0; $i < 2; $i++) {
                    ?>
                        <input id="radioBtn" type="radio" name='gender' class="gender" value= "$i"  >
                        <?php echo $gioiTinh[$i] ?>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <div id="infoDiv">
                <label id="label" class="h-100">Phân khoa </label>
                <select id="input" class="h-100" name="faculty">
                    <!-- required="" oninvalid="this.setCustomValidity('Hãy chọn phân khoa của bạn')" oninput="setCustomValidity('')" -->
                    <?php foreach ($khoa  as $i) : ?>
                        <option><?php echo $i ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div id="dateDiv">
                <label id="label" class="h-100" for="birthday">Ngày sinh </label>
                <!-- <input id="input" class="h-100" type="text" name="date" placeholder="dd/mm/yyyy"> -->
                <td height = '40px'>
                    <input type='date' id="input" class="h-100" name="date" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#ADD8E6'>
                </td>
                <!-- required="" oninvalid="this.setCustomValidity('Hãy nhập ngày sinh đúng định dạng')" oninput="setCustomValidity('')" -->
                <script>
                    $(document).ready(function() {
                        $("#input").datepicker({
                            format: 'dd-mm-yyyy' //can also use format: 'dd-mm-yyyy'     
                        });
                    });
                </script>

            </div>
            <div id="addressDiv">
                <label id="label" class="h-100" for="address">Địa Chỉ </label>
                <input id="input" class="h-100" type="text" name="address">
            </div>
            <div id="btnDiv">
                <button class="btn btn-success" id="submitId" >Đăng ký</button>
            </div>

        </form>
    </div>

</body>

</html>